﻿using UnityEngine;

public enum AntiAircraftGunPosition {
    Thirty,
    Sixty,
    Ninety
};

public class AntiAircraftGun : MonoBehaviour {

    private const int MAX_NUMBER_OF_BULLETS = 5;
    [SerializeField] private Sprite[] _gunSprites = new Sprite[3];
    [SerializeField] private GameObject[] _bulletSpawnPoints = new GameObject[3];
    [SerializeField] private float _bulletForce;
    private SpriteRenderer _spriteRenderer;
    private int _currentSpriteIndex;
    private GameObject _bulletPrefab;
    private int _currentNumberOfBullets;
    private Color _currentSpriteColour;
    private Material _material;

    private void Start() {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _currentSpriteColour = Color.white;
        _material = _spriteRenderer.material;
        _spriteRenderer.sharedMaterial = _material;
        _currentSpriteIndex = 1; //60 degrees
        _bulletPrefab = Resources.Load<GameObject>("Prefabs/bullet");
        _currentNumberOfBullets = MAX_NUMBER_OF_BULLETS;
        Bullet.OnBulletDestroyed += OnBulletDestroyed;

    }

    private void OnDestroy() {
        //using renderer.material creates a copy of the material - so must destroy to avoid memory leak
        Destroy(_material);
    }

    public void RefillGunToMax() {
        _currentNumberOfBullets = MAX_NUMBER_OF_BULLETS;
    }

    public void SetPosition(AntiAircraftGunPosition position) {
        if (GameController.CurrentGameState != GameState.RoundActive) {
            return;
        }

        switch (position) {
            case AntiAircraftGunPosition.Thirty:
                SetSprite(0);
                break;
            case AntiAircraftGunPosition.Sixty:
                SetSprite(1);
                break;
            case AntiAircraftGunPosition.Ninety:
                SetSprite(2);
                break;
        }
    }

    public void Fire() {
        if (_currentNumberOfBullets == 0 || GameController.CurrentGameState != GameState.RoundActive) {
            return;
        }

        int bulletZAngle = 0;
        switch (_currentSpriteIndex) {
            case 0:
                bulletZAngle = 75;
                break;
            case 1:
                bulletZAngle = 60;
                break;
            case 2:
                bulletZAngle = 0;
                break;
        }

        GameObject bullet = Instantiate(_bulletPrefab, _bulletSpawnPoints[_currentSpriteIndex].transform.position, Quaternion.identity);
        bullet.transform.Rotate(new Vector3(0, 0, -bulletZAngle));
        Vector2 firingAngle = new Vector2(bullet.transform.up.x, bullet.transform.up.y);
        bullet.GetComponent<Rigidbody2D>().velocity = firingAngle * _bulletForce;
        _currentNumberOfBullets--;
        UpdateSpriteColor();
    }

    private void SetSprite(int spriteIndex) {
        if (spriteIndex != _currentSpriteIndex) {
            if (_spriteRenderer != null) {
                _spriteRenderer.sprite = _gunSprites[spriteIndex];
                _currentSpriteIndex = spriteIndex;
            }
        }
    }

    private void OnBulletDestroyed() {
        _currentNumberOfBullets++;
        UpdateSpriteColor();
    }

    private void UpdateSpriteColor() {
        if (_currentNumberOfBullets == 0 && _currentSpriteColour == Color.white) {
            _currentSpriteColour = Color.red;
        } else if (_currentNumberOfBullets > 0 && _currentSpriteColour == Color.red) {
            _currentSpriteColour = Color.white;
        }

        _spriteRenderer.sharedMaterial.color = _currentSpriteColour;
    }
}
