﻿using UnityEngine;

public class AntiAircraftGunController : MonoBehaviour {

    private AntiAircraftGun _antiAircraftGun;

    public void SpawnAntiAcraftGun() {
        if (_antiAircraftGun == null) {
            Camera camera = Camera.main;
            GameObject prefab = Resources.Load<GameObject>("Prefabs/antiAircraftGun");
            float zPosition = FindObjectOfType<PlaneSpawner>().transform.position.z;
            Vector3 position = camera.ScreenToWorldPoint(new Vector3(Screen.width / 4, Screen.height / 5));
            position.z = zPosition;
            GameObject gunGameObject = Instantiate(prefab, position, Quaternion.identity);
            _antiAircraftGun = gunGameObject.GetComponent<AntiAircraftGun>();
        }
    }

    public void RefillAircraftGun() {
        _antiAircraftGun.RefillGunToMax();
    }

    public void ClearAllBulletsFromScene() {
        var bullets = FindObjectsOfType<Bullet>();
        for (int i = 0; i < bullets.Length; i++) {
            Destroy(bullets[i].gameObject);
        }
    }

    private void Update() {
        CheckInput();
    }

    private void CheckInput() {
        if (Input.GetKey(KeyCode.UpArrow)) {
            _antiAircraftGun?.SetPosition(AntiAircraftGunPosition.Thirty);
        } else if (Input.GetKey(KeyCode.DownArrow)) {
            _antiAircraftGun?.SetPosition(AntiAircraftGunPosition.Ninety);
        } else {
            _antiAircraftGun?.SetPosition(AntiAircraftGunPosition.Sixty);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            _antiAircraftGun?.Fire();
        }
    }
}
