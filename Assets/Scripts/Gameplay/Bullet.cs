﻿using UnityEngine;

public class Bullet : MonoBehaviour {
    public delegate void BulletDestroyed();
    public static event BulletDestroyed OnBulletDestroyed;
    public delegate void BulletHitPlane(GameObject plane);
    public static event BulletHitPlane OnBulletHitPlane;

    SpriteRenderer _spriteRenderer;
    Rigidbody2D _rigidbody;

    // Start is called before the first frame update
    private void Start() {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update() {
        CheckIfOutOfBounds();

        if (GameController.CurrentGameState != GameState.RoundActive) {
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.isKinematic = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Plane") {
            OnBulletHitPlane?.Invoke(collision.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnDestroy() {
        OnBulletDestroyed?.Invoke();
    }

    private void CheckIfOutOfBounds() {
        if (!_spriteRenderer.isVisible) {
            Destroy(gameObject);
        }
    }
}
