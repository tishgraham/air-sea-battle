﻿using UnityEngine;

public enum GameState {
    MainMenu,
    RoundActive,
    RoundOver
};

public class GameController : MonoBehaviour {
    public static GameState CurrentGameState { get; private set; }

    private UIController _uiController;
    private PlaneFormationController _planeFormationController;
    private RoundTimer _roundTimer;
    private AntiAircraftGunController _antiAircraftGunController;
    private ScoreController _scoreController;

    private void Start() {
        CurrentGameState = GameState.MainMenu;
        _uiController = GetComponent<UIController>();
        _planeFormationController = GetComponent<PlaneFormationController>();
        _roundTimer = GetComponent<RoundTimer>();
        _scoreController = GetComponent<ScoreController>();
        _antiAircraftGunController = GetComponent<AntiAircraftGunController>();
        RoundTimer.TimerEnded += OnTimerEnded;
    }

    private void OnDestroy() {
        RoundTimer.TimerEnded -= OnTimerEnded;
    }

    public void SetCurrentGameState(GameState state) {
        CurrentGameState = state;
    }

    public void StartRound() {
        _uiController.ToggleMainMenuPanel(false);
        _uiController.ToggleActiveRoundPanel(true);
        _antiAircraftGunController.SpawnAntiAcraftGun();
        _planeFormationController.SpawnNewFormationOfRandomSize();
        _roundTimer.StartTimer();
        CurrentGameState = GameState.RoundActive;
    }

    public void Retry() {
        _planeFormationController.DestroyAllPlanesInFormation();
        _scoreController.ClearScore();
        _antiAircraftGunController.RefillAircraftGun();
        _antiAircraftGunController.ClearAllBulletsFromScene();
        _uiController.ToggleRoundOverPanel(false);
        StartRound();
    }

    private void OnTimerEnded() {
        CurrentGameState = GameState.RoundOver;
        _uiController.ToggleActiveRoundPanel(false);
        _uiController.ToggleRoundOverPanel(true);
        _uiController.SetScoreTextOnRoundOverPanel(_scoreController.GetCurrentScore());
    }


}
