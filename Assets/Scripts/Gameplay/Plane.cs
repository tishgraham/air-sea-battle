﻿using UnityEngine;

public class Plane : MonoBehaviour {
    public delegate void OnPlaneDestroyed(GameObject plane, bool destroyedInActiveGame);
    public static event OnPlaneDestroyed PlaneDestroyed;

    [SerializeField] private GameObject _planeTail;
    [SerializeField] private float _speed;
    private Vector3 _spawningPosition;
    private Vector3 _leftOfScreenPosition;
    private Camera _camera;
    private bool _playSoundOnDestroy;

    private void Start() {
        _spawningPosition = transform.position;
        _camera = Camera.main;
        _playSoundOnDestroy = true;
        CalculateLeftOfScreenRespawnPosition();
        Bullet.OnBulletHitPlane += OnBulletHitPlane;
    }

    private void Update() {
        if (GameController.CurrentGameState == GameState.RoundActive) {
            transform.Translate(new Vector3(_speed * Time.deltaTime, 0, 0));
            CheckIfPlaneIsOutOfBounds();
        }
    }

    private void OnDestroy() {
        Bullet.OnBulletHitPlane -= OnBulletHitPlane;
        PlaneDestroyed?.Invoke(gameObject, _playSoundOnDestroy);
    }

    public void TogglePlaySound(bool value) {
        _playSoundOnDestroy = value;
    }


    private void CheckIfPlaneIsOutOfBounds() {
        if (_camera == null) {
            return;
        }
        Vector3 screenPos = _camera.WorldToScreenPoint(_planeTail.transform.position);

        if (screenPos.x > Screen.width) {
            transform.position = _leftOfScreenPosition;
        }
    }

    private void CalculateLeftOfScreenRespawnPosition() {
        var leftScreenBoundPosition = _camera.ScreenToWorldPoint(new Vector3(0, 0, _spawningPosition.z));
        _leftOfScreenPosition = new Vector3(leftScreenBoundPosition.x, _spawningPosition.y, _spawningPosition.z);
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        var spriteBounds = renderer.sprite.bounds;
        _leftOfScreenPosition.x -= spriteBounds.extents.x;
    }

    private void OnBulletHitPlane(GameObject plane) {
        if (plane == gameObject) {
            Destroy(gameObject);
        }
    }
}
