﻿using UnityEngine;

public class PlaneFormationController : MonoBehaviour {

    [SerializeField] private int _minFormationSize = 3;
    [SerializeField] private int _maxFormationSize = 5;
    [SerializeField] private PlaneSpawner[] _planeSpawners;

    private int _numberOfSpawnPoints;
    private GameObject[] _planesInFormation;

    private void Start() {
        Plane.PlaneDestroyed += OnPlaneDestroyed;
    }

    private void OnDestroy() {
        Plane.PlaneDestroyed -= OnPlaneDestroyed;
    }

    private void Update() {
        if (GetNumberOfPlanesInCurrentFormation() == 0 && GameController.CurrentGameState == GameState.RoundActive) {
            SpawnNewFormationOfRandomSize();
        }
    }

    public int GetNumberOfSpawnPoints() {
        return _planeSpawners.Length;
    }

    public void GetSpawners() {
        if (_planeSpawners == null || _planeSpawners.Length == 0) {
            _planeSpawners = FindObjectsOfType<PlaneSpawner>();
        }
    }

    public int GetNumberOfPlanesInCurrentFormation() {
        if (_planesInFormation == null) {
            return -1;
        }

        int planes = 0;
        for (int i = 0; i < _planesInFormation.Length; i++) {
            if (_planesInFormation[i] != null) {
                planes++;
            }
        }

        return planes;
    }

    public GameObject GetPlaneFromFormation(int formationIndex) {
        return _planesInFormation[formationIndex];
    }

    public void SpawnNewFormationOfRandomSize() {
        int formationSize = Random.Range(_minFormationSize, _maxFormationSize + 1); //random.range is max exclusive when using ints, so +1 to make sure that we can get max formation size
        SpawnNewFormation(formationSize);
    }

    public void SpawnFormationOfFixedSize(int formationSize) {
        SpawnNewFormation(formationSize);
    }

    public void DestroyAllPlanesInFormation() {
        for (int i = 0; i < _planesInFormation.Length; i++) {
            if (_planesInFormation[i] != null) {
                _planesInFormation[i].GetComponent<Plane>().TogglePlaySound(false);
                Destroy(_planesInFormation[i]);
                _planesInFormation[i] = null;
            }
        }
    }

    private void SpawnNewFormation(int formationSize) {
        //pick number of planes based on number of spawners
        //create array of spawners to use for formation - pick at random and don't reuse the same spawner
        //spawn plane at each spawner
        if (_planesInFormation != null) {
            ClearCurrentFormation();
        } else {
            _planesInFormation = new GameObject[_maxFormationSize];
        }
        PlaneSpawner[] formationSpawners = new PlaneSpawner[formationSize];

        for (int i = 0; i < formationSize; i++) {
            formationSpawners[i] = PickRandomSpawner(formationSpawners);
        }

        for (int i = 0; i < formationSpawners.Length; i++) {
            _planesInFormation[i] = formationSpawners[i].SpawnPlane();
        }
    }

    private PlaneSpawner PickRandomSpawner(PlaneSpawner[] formationSpawnPoints) {
        GetSpawners();
        PlaneSpawner randomSpawnPoint = _planeSpawners[Random.Range(0, _planeSpawners.Length)];

        for (int i = 0; i < formationSpawnPoints.Length; i++) {
            if (formationSpawnPoints[i] == randomSpawnPoint) {
                randomSpawnPoint = PickRandomSpawner(formationSpawnPoints);
            }
        }

        return randomSpawnPoint;
    }

    private void ClearCurrentFormation() {

        for (int i = 0; i < _planesInFormation.Length; i++) {
            _planesInFormation[i] = null;
        }
    }

    private void OnPlaneDestroyed(GameObject plane, bool activeGame) {
        for (int i = 0; i < _planesInFormation.Length; i++) {
            if (_planesInFormation[i] == plane) {
                _planesInFormation[i] = null;
            }
        }
    }
}
