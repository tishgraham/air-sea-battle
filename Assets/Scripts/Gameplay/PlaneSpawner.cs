﻿using UnityEngine;

public class PlaneSpawner : MonoBehaviour {
    [SerializeField] private GameObject _planePrefab;

    public GameObject SpawnPlane() {
        return Instantiate(_planePrefab, transform.position, Quaternion.identity);
    }
}
