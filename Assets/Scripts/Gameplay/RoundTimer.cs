﻿using UnityEngine;
using UnityEngine.UI;

public class RoundTimer : MonoBehaviour {
    public delegate void OnTimerEnded();
    public static event OnTimerEnded TimerEnded;

    [SerializeField] private int roundTimeInSeconds;
    [SerializeField] private Text _timerText;
    private bool _timerStarted = false;
    private float _currentTime = 0;

    private void Update() {
        if (GameController.CurrentGameState == GameState.RoundActive && _currentTime > 0) {
            _currentTime -= Time.deltaTime;
            _timerText.text = GetCurrentTimeInSeconds().ToString();
        } else if (_timerStarted && _currentTime <= 0) {
            _timerStarted = false;
            TimerEnded?.Invoke();
        }
    }

    public void StartTimer() {
        SetTimer(roundTimeInSeconds);
        _timerStarted = true;
    }

    public void SetTimer(float time) {
        _currentTime = time;
    }

    public float GetCurrentTimeInSeconds() {
        float time = Mathf.FloorToInt(_currentTime);
        return time;
    }
}
