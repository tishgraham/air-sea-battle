﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {
    [SerializeField] private Text _scoreText;
    [SerializeField] private int _pointsForDestroyingPlane;
    private int _currentScore;

    void Start() {
        ClearScore();
        Plane.PlaneDestroyed += OnPlaneDestroyed;
    }

    private void OnDestroy() {
        Plane.PlaneDestroyed -= OnPlaneDestroyed;
    }

    public int GetCurrentScore() {
        return _currentScore;
    }

    public void ClearScore() {
        _currentScore = 0;
        UpdateScoreText();
    }

    private void AddToScore(int value) {
        _currentScore += value;
        UpdateScoreText();
    }

    private void UpdateScoreText() {
        _scoreText.text = _currentScore.ToString();
    }

    private void OnPlaneDestroyed(GameObject plane, bool activeGame) {
        if(activeGame) AddToScore(_pointsForDestroyingPlane);
    }

}
