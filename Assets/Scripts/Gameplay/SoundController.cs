﻿using UnityEngine;

public class SoundController : MonoBehaviour {
    [SerializeField] private AudioClip _explosionClip;
    private AudioSource _audioSource;

    private void Start() {
        _audioSource = GetComponent<AudioSource>();
        Plane.PlaneDestroyed += OnPlaneDestroyed;
    }

    private void OnDestroy() {
        Plane.PlaneDestroyed -= OnPlaneDestroyed;
    }

    private void OnPlaneDestroyed(GameObject plane, bool activeGame) {
        if (activeGame) {
            PlayExplosionClip();
        }
    }

    private void PlayExplosionClip() {
        _audioSource.PlayOneShot(_explosionClip);
    }
}
