﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    [SerializeField] private GameObject _uiCanvasGameObject;
    private GameObject _mainMenuPanel;
    private GameObject _activeRoundPanel;
    private GameObject _roundOverPanel;

    private void Start() {
        _uiCanvasGameObject = GetComponentInChildren<Canvas>().gameObject;
        GetPanels();
        ToggleActiveRoundPanel(false);
        ToggleRoundOverPanel(false);
    }

    public void ToggleMainMenuPanel(bool value) {
        TogglePanel(_mainMenuPanel, value);
    }

    public void ToggleActiveRoundPanel(bool value) {
        TogglePanel(_activeRoundPanel, value);
    }

    public void ToggleRoundOverPanel(bool value) {
        TogglePanel(_roundOverPanel, value);
    }

    public void SetScoreTextOnRoundOverPanel(int value) {
        var textObjects = _roundOverPanel.GetComponentsInChildren<Text>();
        for (int i = 0; i < textObjects.Length; i++) {
            if (textObjects[i].gameObject.name == "ScoreText") {
                textObjects[i].text = $"Score: {value}";
            }
        }
    }

    private void GetPanels() {
        var uiObjects = GetComponentsInChildren<RectTransform>();

        for (int i = 0; i < uiObjects.Length; i++) {
            switch (uiObjects[i].name) {
                case "MainMenuPanel":
                    _mainMenuPanel = uiObjects[i].gameObject;
                    break;
                case "ActiveRoundPanel":
                    _activeRoundPanel = uiObjects[i].gameObject;
                    break;
                case "RoundOverPanel":
                    _roundOverPanel = uiObjects[i].gameObject;
                    break;

            }
        }
    }

    private void TogglePanel(GameObject panel, bool value) {
        panel.SetActive(value);
    }

}
