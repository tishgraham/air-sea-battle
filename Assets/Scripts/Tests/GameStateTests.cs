﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class GameStateTests {
        private GameController _gameController;

        [SetUp]
        public void Setup() {
            _gameController = TestUtils.SetUpLevel();
        }

        [TearDown]
        public void TearDown() {
            TestUtils.DestroyAllGameObjectsInScene();
        }

        [UnityTest]
        public IEnumerator WhenGameStateIsRoundOver_PlanesDoNotMove() {
            _gameController.StartRound();
            var plane = MonoBehaviour.FindObjectOfType<Plane>();
            var planePosition = plane.transform.position;
            _gameController.SetCurrentGameState(GameState.RoundOver);
            yield return new WaitForSeconds(0.1f);

            Assert.True(planePosition == plane.transform.position);
        }

        [UnityTest]
        public IEnumerator WhenGameStateIsRoundOver_TimerStopsCountingDown() {
            _gameController.StartRound();
            var timer = MonoBehaviour.FindObjectOfType<RoundTimer>();
            var time = timer.GetCurrentTimeInSeconds();
            _gameController.SetCurrentGameState(GameState.RoundOver);
            yield return new WaitForSeconds(1);

            Assert.True(time == timer.GetCurrentTimeInSeconds());
        }

        [UnityTest]
        public IEnumerator WhenGameStateIsRoundOver_GunCannotFire() {
            _gameController.StartRound();
            var gun = MonoBehaviour.FindObjectOfType<AntiAircraftGun>();
            _gameController.SetCurrentGameState(GameState.RoundOver);
            gun.Fire();

            yield return new WaitForEndOfFrame();

            Assert.True(MonoBehaviour.FindObjectsOfType<Bullet>().Length == 0);
        }

    }
}
