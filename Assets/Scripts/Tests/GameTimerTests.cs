﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class GameTimerTests {
        private GameController _gameController;
        private RoundTimer _roundTimer;

        [SetUp]
        public void Setup() {
            _gameController = TestUtils.SetUpLevel();
            _roundTimer = _gameController.gameObject.GetComponent<RoundTimer>();
        }

        [TearDown]
        public void TearDown() {
            TestUtils.DestroyAllGameObjectsInScene();
        }

        [UnityTest]
        public IEnumerator WhenGameStarts_TimerIsNot0Seconds() {
            _gameController.StartRound();
            yield return new WaitForEndOfFrame();
            Assert.True(_roundTimer.GetCurrentTimeInSeconds() > 0);
        }

        [UnityTest]
        public IEnumerator WhenGameIsInProgress_TimerCountsDown() {
            _gameController.StartRound();
            float time = _roundTimer.GetCurrentTimeInSeconds();
            yield return new WaitForSeconds(1);

            Assert.True(_roundTimer.GetCurrentTimeInSeconds() < time);
        }

        [UnityTest]
        public IEnumerator WhenTimerReaches0_GameStateIsSetToRoundOver() {
            _gameController.StartRound();
            _roundTimer.SetTimer(0);
            yield return new WaitForSeconds(0.1f);

            Assert.True(GameController.CurrentGameState == GameState.RoundOver);
        }
    }
}
