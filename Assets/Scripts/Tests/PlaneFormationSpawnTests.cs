﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class PlaneFormationSpawningTests {

        private GameController _gameController;
        private PlaneFormationController _planeFormationController;

        [SetUp]
        public void Setup() {
            _gameController = TestUtils.SetUpLevel();
            _planeFormationController = _gameController.gameObject.GetComponent<PlaneFormationController>();
        }

        [TearDown]
        public void TearDown() {
            TestUtils.DestroyAllGameObjectsInScene();
        }

        [Test]
        public void WhenGameStarts_ThereAre5OrMorePlaneSpawnersPresentInLevel() {
            _planeFormationController.GetSpawners();
            int numberOfSpawnPoints = _planeFormationController.GetNumberOfSpawnPoints();
            Assert.GreaterOrEqual(numberOfSpawnPoints, 5);
        }

        [UnityTest]
        public IEnumerator WhenGameStarts_AFormationSpawns() {
            _gameController.StartRound();
            yield return new WaitForSeconds(0.1f);
            Assert.NotZero(_planeFormationController.GetNumberOfPlanesInCurrentFormation());
        }

        [UnityTest]
        public IEnumerator WhenFormationSpawns_3OrMorePlanesSpawn() {
            _gameController.StartRound();
            yield return new WaitForSeconds(0.1f);
            Assert.GreaterOrEqual(_planeFormationController.GetNumberOfPlanesInCurrentFormation(), 3);
        }

        [UnityTest]
        public IEnumerator WhenFormationSpawns_5OrLessPlanesSpawn() {
            _gameController.StartRound();
            yield return new WaitForSeconds(0.1f);
            Assert.LessOrEqual(_planeFormationController.GetNumberOfPlanesInCurrentFormation(), 5);
        }

        [UnityTest]
        public IEnumerator WhenFormationSpawns_NoPlanesShareASpawnPoint() {
            _gameController.StartRound();
            yield return new WaitForSeconds(0.1f);

            bool sharingSpawners = false;
            int formationSize = _planeFormationController.GetNumberOfPlanesInCurrentFormation();
            for (int i = 0; i < formationSize; i++) {
                GameObject planeA = _planeFormationController.GetPlaneFromFormation(i);
                for (int j = 0; j < formationSize; j++) {
                    GameObject planeB = _planeFormationController.GetPlaneFromFormation(i);

                    if (planeA == planeB) {
                        continue;
                    } else if (planeA.transform.position == planeB.transform.position) {
                        sharingSpawners = true;
                        break;
                    }
                }
            }

            Assert.False(sharingSpawners);
        }

        [UnityTest]
        public IEnumerator WhenLastPlaneOfFormationIsDestroyed_NewFormationSpawns() {
            _gameController.SetCurrentGameState(GameState.RoundActive);
            _planeFormationController.SpawnFormationOfFixedSize(1);
            yield return new WaitForSeconds(0.1f);

            var plane = _planeFormationController.GetPlaneFromFormation(0);
            GameObject.Destroy(plane);
            yield return new WaitForSeconds(0.5f);

            Assert.NotZero(_planeFormationController.GetNumberOfPlanesInCurrentFormation());
        }

        [UnityTest]
        public IEnumerator WhenPlaneSpawns_PlaneSpriteIsPresent() {
            _gameController.StartRound();
            yield return new WaitForSeconds(0.1f);

            GameObject plane = _planeFormationController.GetPlaneFromFormation(0);
            SpriteRenderer planeSpriteRenderer = plane.GetComponent<SpriteRenderer>();
            Assert.IsTrue(planeSpriteRenderer.sprite.name == "plane");
        }

        [UnityTest]
        public IEnumerator WhenPlaneSpawns_PlaneComponentIsPresent() {
            _gameController.StartRound();
            yield return new WaitForSeconds(0.1f);

            GameObject plane = _planeFormationController.GetPlaneFromFormation(0);
            Assert.NotNull(plane.GetComponent<Plane>());
        }
    }
}
