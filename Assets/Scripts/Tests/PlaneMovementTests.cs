﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class PlaneMovementTests {
        private GameController _gameController;
        private PlaneFormationController _planeFormationController;

        [SetUp]
        public void Setup() {
            _gameController = TestUtils.SetUpLevel();
            _planeFormationController = _gameController.gameObject.GetComponent<PlaneFormationController>();
            _planeFormationController.SpawnFormationOfFixedSize(1);
        }

        [TearDown]
        public void TearDown() {
            TestUtils.DestroyAllGameObjectsInScene();
        }

        [UnityTest]
        public IEnumerator WhenPlaneIsSpawned_PlaneMovesTowardsTheRightSideOfTheScreen() {
            _gameController.SetCurrentGameState(GameState.RoundActive);
            GameObject planeGameObject = _planeFormationController.GetPlaneFromFormation(0);
            Vector3 oldPosition = planeGameObject.transform.position;
            yield return new WaitForSeconds(0.5f);

            Assert.Less(oldPosition.x, planeGameObject.transform.position.x);
        }

        [UnityTest]
        public IEnumerator WhenPlaneLeavesRightSideOfScreen_PlaneReturnsToLeftSideOfScreen() {
            _gameController.SetCurrentGameState(GameState.RoundActive);
            GameObject planeGameObject = _planeFormationController.GetPlaneFromFormation(0);
            Camera camera = Camera.main;
            planeGameObject.transform.position = new Vector3(camera.ViewportToWorldPoint(new Vector3(Screen.width, Screen.height)).x, planeGameObject.transform.position.y, planeGameObject.transform.position.z);
            yield return new WaitForEndOfFrame();

            Assert.Greater(planeGameObject.transform.position.x, camera.ViewportToWorldPoint(new Vector3(0, 0)).x);
        }

    }
}
