﻿using UnityEngine;

namespace Tests {
    public static class TestUtils {

        public static GameController SetUpLevel() {
            var _gameCoreObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameCore"));
            var _cameraObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Main Camera"));
            var camera = _cameraObject.GetComponent<Camera>();
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/PlaneSpawners"), camera.ScreenToWorldPoint(new Vector3(0, Screen.height / 0.75f)), Quaternion.identity);
            return _gameCoreObject.GetComponent<GameController>();
        }

        public static void DestroyAllGameObjectsInScene() {
            var gameObjects = MonoBehaviour.FindObjectsOfType<GameObject>();
            for (int i = 0; i < gameObjects.Length; i++) {
                MonoBehaviour.Destroy(gameObjects[i]);
            }
        }
    }
}
