Decisions Made:
- Build one feature at a time and occompanied tests at the same time as the feature
- Focus on functionality first and then iterate
- List numerous tests related to the areas mentioned in the Automated Tests section

Assumptions Made:
- It wasn't clear how to handle limiting the 5 projectiles on the screen. I decided to limit the shots 
that could be fired from the anti-aircraft gun to 5, and when one was destroyed (by striking a plane or 
going outside of the screen bounds) the bullet was 'reloaded' into the gun. I added a color change to
the gun sprite for when it was 'out of bullets' to attempt to notify the player that there was a reason
they could no longer fire the gun.

- It wasn't clear from the sprite alone where the bullet should be firing from when the gun was at the 60
degree angle. At 90 and 30 I used the flat end of the turret part of the sprite to determine the correct 
tragectory. For the 60 degree angle I used the corner of the turret part of the sprite.

- It was assumed that this could be used for expanding the prototype for 2nd player input, so I tried to 
keep input seperate to functionality of the anti-aircraft gun so that controls could be changed / faked for tests
more easily.


Issues Found:
- Some of the tests were a bit fiddly to write - I think because the set up of the game/ui/sound/plane controllers. When I tried to set up Setup/Teardowns, I found that I couldn't start games
from the Startup method because the game controller hadn't found the other components it needed in time before 
start game is called. Initially, I thought it was because I had tried to load the whole game scene before running 
each test - but the issue still remained after changing the test structure to build levels on the fly (which I think
was a better set up). To work around this, I had to call start game from each test - which worked but
was not ideal.

- Calculating the tragectory of the bullets was also fiddly - as I'd selected to use Unity2D for the project and
was using mainly 2D assets, I didn't have the same functionality I'd try to use in a 3D environment. I wanted
to rotate the bullet and then use it's forward vector to push it in the direction it was facing, but I found 
an alternate way with a bit of googling.

- I had some problems with the Unity Test Framework plugin itself - on numerous occasions, the plugin needed
to be uninstalled and reinstalled because it was throwing compile errors for editor windows. I also could not
make a local build of the game, as I was getting plugin collisions for 'nunit.framework.dll'. The error was as follows:

"Plugin 'nunit.framework.dll' is used from several locations:

 Packages/com.unity.ext.nunit/net35/unity-custom/nunit.framework.dll would be copied to <PluginPath>/nunit.framework.dll

 C:/Program Files/Unity.2019.1.8/Editor/Data/UnityExtensions/Unity/TestRunner/net35/unity-custom/nunit.framework.dll would be copied to <PluginPath>/nunit.framework.dll

Please fix plugin settings and try again.

UnityEditor.Modules.DefaultPluginImporterExtension:CheckFileCollisions(String)
UnityEditorInternal.PluginsHelper:CheckFileCollisions(BuildTarget) (at C:/buildslave/unity/build/Editor/Mono/Plugins/PluginsHelper.cs:25)
UnityEditor.BuildPlayerWindow:BuildPlayerAndRun()"

When I looked in the Packages folder, there was no folder that matched the description in the error, and when I removed other
instances of the package from the project and other locations on my PC - unity editor would throw different compile errors.
After a couple of hours of debugging locally and googling for solutions, I could not fix it. If
this were to happen at work, I would be asking for help at this point. (If you know what this issue is, please let me know -
it was driving me nuts!)

Estimated Time: 16 - 18 hours

Third Part Assets Used: None